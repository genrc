/* This file is part of genrc
Copyryght (C) 2018-2024 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include "genrc.h"
#include <pcre.h>

static char *
progname_pattern(char const *prog)
{
	grecs_txtacc_t acc;
	char *ret;
	static char const specials[] = "\\[]?.*+(){}|";

	acc = grecs_txtacc_create();
	grecs_txtacc_grow_char(acc, '^');
	if (prog[0] != '/')
		grecs_txtacc_grow_string(acc, "(?:/(?:.*/)?)?");
	for (; *prog; prog++) {
		if (strchr(specials, *prog))
			grecs_txtacc_grow_char(acc, '\\');
		grecs_txtacc_grow_char(acc, *prog);
	}
	grecs_txtacc_grow_char(acc, '$');
	grecs_txtacc_grow_char(acc, 0);
	ret = grecs_txtacc_finish(acc, 1);
	grecs_txtacc_free(acc);
	return ret;
}

void
match_pcre_init(PROCSCANBUF buf, char const *pattern)
{
	pcre *pcre;
	char const *error;
	int error_offset;
	char *tmp = NULL;
	int cflags = 0;
	if (buf->flags & PROCF_ICASE)
		cflags |= PCRE_CASELESS;
	if (!pattern) {
		pattern = tmp = progname_pattern(genrc_program);
	}
	pcre = pcre_compile(pattern, cflags, &error, &error_offset, 0);
	free(tmp);
	if (!pcre)
		usage_error("%s at offset %d", error, error_offset);
	buf->pattern = pcre;
}

void
match_pcre_free(PROCSCANBUF buf)
{
	pcre_free ((pcre*)buf->pattern);
}

int
match_pcre(PROCSCANBUF buf, char const *arg)
{
	int rc = pcre_exec((pcre*)buf->pattern, 0, arg, strlen(arg),
			   0, 0, NULL, 0);
	return !(rc >= 0);
	/* FIXME: error message unless rc == PCRE_ERROR_NOMATCH */
}
