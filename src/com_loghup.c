/* This file is part of genrc
Copyryght (C) 2024 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include "genrc.h"

int
com_loghup(void)
{
	PIDLIST pids;
	size_t i;
	pid_t ppid = -1;
	
	pidlist_init(&pids);
	if (get_pid_list(genrc_pid_closure, &pids))
		return 1;

	for (i = 0; i < pids.pidc; i++) {
		pid_t pid = get_genrc_pid(pids.pidv[i]);
		if (pid != -1) {
			if (ppid == -1)
				ppid = pid;
			else if (ppid != pid) {
				ppid = -1;
				break;
			}
		}
	}

	pidlist_free(&pids);
	if (ppid == -1) {
		genrc_error("cannot reliably determine pid of the running process");
		return 1;
	}

	if (kill(ppid, SIGHUP)) {
		system_error(errno, "signalling %ld", (long)ppid);
		return 1;
	}
	
	return 0;
}
