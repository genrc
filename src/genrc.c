/* This file is part of genrc
Copyryght (C) 2018-2024 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include "genrc.h"
#include <syslog.h>
#include <sys/ioctl.h>
#include <sys/resource.h>

char *genrc_command;
char *genrc_program;
char *genrc_pid_from;
unsigned genrc_start_timeout = 5;
unsigned genrc_stop_timeout = 5;
int genrc_no_reload;
int genrc_signal_stop = SIGTERM;
int genrc_signal_reload = SIGHUP;
enum genrc_kill_mode genrc_kill_mode = genrc_kill_process;
GENRC_PID_CLOSURE *genrc_pid_closure;
char *genrc_create_pidfile;
int genrc_verbose = 1;
char *genrc_shell = SHELL;
int genrc_log_facility = LOG_DAEMON;
char *genrc_log_tag = NULL;

enum {
	OPT_USAGE = 256,
	OPT_VERSION,
	OPT_SIGNAL_RELOAD,
	OPT_NO_RELOAD,
	OPT_SIGNAL_STOP,
	OPT_CREATE_PIDFILE,
	OPT_RESTART_ON_EXIT,
	OPT_RESTART_ON_SIGNAL,
	OPT_LOG_FACILITY,
	OPT_LOG_TAG,
	OPT_LOG_ERR_FILE,
	OPT_LOG_OUT_FILE,
	OPT_LOG_FILE
};

struct option longopts[] = {
	{ "help",              no_argument,       0, 'h' },
	{ "usage",             no_argument,       0, OPT_USAGE },
	{ "command",           required_argument, 0, 'c' },
	{ "directory",         required_argument, 0, 'C' },
	{ "program",           required_argument, 0, 'p' },
	{ "pid-from",          required_argument, 0, 'P' },
	{ "pidfile",           required_argument, 0, 'F' },
	{ "timeout",           required_argument, 0, 't' },
	{ "signal-reload",     required_argument, 0, OPT_SIGNAL_RELOAD },
	{ "no-reload",         no_argument,       0, OPT_NO_RELOAD },
	{ "signal-stop",       required_argument, 0, OPT_SIGNAL_STOP },
	{ "sentinel",          no_argument,       0, 'S' },
	{ "shell",             no_argument,       0, 's' },
	{ "exec",              no_argument,       0, 'e' },
	{ "create-pidfile",    required_argument, 0, OPT_CREATE_PIDFILE },
	{ "version",           no_argument,       0, OPT_VERSION },
	{ "verbose",           no_argument,       0, 'v' },
	{ "user",              required_argument, 0, 'u' },
	{ "group",             required_argument, 0, 'g' },
	{ "restart-on-exit",   required_argument, 0, OPT_RESTART_ON_EXIT },
	{ "restart-on-signal", required_argument, 0, OPT_RESTART_ON_SIGNAL },
	{ "log-facility",      required_argument, 0, OPT_LOG_FACILITY },
	{ "log-tag",           required_argument, 0, OPT_LOG_TAG },
	{ "log-err-file",      required_argument, 0, OPT_LOG_ERR_FILE },
	{ "log-out-file",      required_argument, 0, OPT_LOG_OUT_FILE },
	{ "log-file",          required_argument, 0, OPT_LOG_FILE },
	{ "limit",             required_argument, 0, 'l' },
	{ "kill-mode",         required_argument, 0, 'k' },
	{ "quiet",             no_argument,       0, 'q' },
	{ NULL }
};
char shortopts[2*sizeof(longopts)/sizeof(longopts[0])];

static void
shortopts_setup(void)
{
	int i;
	char *p;

	for (i = 0, p = shortopts; longopts[i].name; i++) {
		if (longopts[i].val < 128) {
			*p++ = longopts[i].val;
			if (longopts[i].has_arg != no_argument) {
				*p++ = ':';
				if (longopts[i].has_arg == optional_argument)
					*p++ = ':';
			}
		}
	}
	*p = 0;
}


struct sigdefn {
	char const *sig_name;
	int sig_no;
};

#define S(s) { #s, s }
static struct sigdefn sigdefn[] = {
  S (SIGHUP),
  S (SIGINT),
  S (SIGQUIT),
  S (SIGILL),
  S (SIGTRAP),
  S (SIGABRT),
  S (SIGIOT),
  S (SIGBUS),
  S (SIGFPE),
  S (SIGKILL),
  S (SIGUSR1),
  S (SIGSEGV),
  S (SIGUSR2),
  S (SIGPIPE),
  S (SIGALRM),
  S (SIGTERM),
#ifdef SIGSTKFLT
  S (SIGSTKFLT),
#endif
  S (SIGCHLD),
  S (SIGCONT),
  S (SIGSTOP),
  S (SIGTSTP),
  S (SIGTTIN),
  S (SIGTTOU),
#ifdef SIGURG
  S (SIGURG),
#endif
#ifdef SIGXCPU
  S (SIGXCPU),
#endif
#ifdef SIGXFSZ
  S (SIGXFSZ),
#endif
#ifdef SIGVTALRM
  S (SIGVTALRM),
#endif
#ifdef SIGPROF
  S (SIGPROF),
#endif
#ifdef SIGWINCH
  S (SIGWINCH),
#endif
#ifdef SIGPOLL
  S (SIGPOLL),
#endif
#ifdef SIGIO
  S (SIGIO),
#endif
#ifdef SIGPWR
  S (SIGPWR),
#endif
#ifdef SIGSYS
  S (SIGSYS),
#endif
  {NULL}
};
#undef S

static int
is_numeric_str(char const *s)
{
	while (*s) {
		if (!isdigit(*s))
			return 0;
		s++;
	}
	return 1;
}

int
str_to_int(char const *s)
{
	char *end;
	unsigned long n;
	errno = 0;
	n = strtoul(s, &end, 10);
	if (errno || *end || n > UINT_MAX)
		return -1;
	return n;
}

int
str_to_sig(char const *s)
{
	if (is_numeric_str(s)) {
		return str_to_int(s);
	} else {
		struct sigdefn *sd;

		for (sd = sigdefn; sd->sig_name; sd++) {
			if (s[0] == 's' || s[0] == 'S') {
				if (strcasecmp(sd->sig_name, s) == 0)
					return sd->sig_no;
			} else if (strcasecmp(sd->sig_name + 3, s) == 0)
				return sd->sig_no;
		}
	}
	return -1;
}

char const *help_msg[] = {
	"Usage: genrc [OPTIONS] COMMAND",
	"generic system initialization script helper\n",
	"COMMANDs are:\n",
	"  start                    start the program",
	"  stop                     stop the program",
	"  restart                  restart the running program",
	"  reload                   send a reload signal to the program",
	"  status                   display the program status",
	"  loghup                   send HUP to the logger process",
	"  wait                     wait for the program to terminate",
	"",
	"OPTIONs are:",
	"",
	"Command to run:",
	"",
	"  -c, --command=COMMAND    command line to run",
	"  -p, --program=PROGRAM    name of the program to run",
	"",
	"   At least one of COMMAND or PROGRAM must be given.",
	"   If PROGRAM is supplied, but COMMAND is not, COMMAND is set to PROGRAM",
	"   Otherwise, if COMMAND is set, but PROGRAM is not, PROGRAM is set to the",
	"   first word (in the shell sense) in COMMAND.",
	"",
	"Runtime privileges and limits:",
	"",
	"  -u, --user=NAME          run with this user privileges",
	"  -g, --group=GROUP[,GROUP...]",
	"                           run with this group(s) privileges",
	"  -l, --limit=LIM          set resource limit; LIM is a resource letter",
	"                           followed by the resource value; resource letters are:",
	"        c       core file size (KB)",
	"        d       data size (KB)",
	"        f       file size (KB)",
	"        l       locked-in-memory address space (KB)",
	"        m       resident set size (KB)",
	"        n       number of open files",
	"        p       process priority -20..19",
	"        s       stack size (KB)",
	"        t       CPU time (seconds)",
	"        u       number of subprocesses",
	"        v       virtual memory (KB)",
	"",
	"        Use upper-case letters to enforce limit setting;",
	"        Several limits can be set in one option, e.g.: -l n128P-10t60",
	"",
	"Additional configuration:",
	"",
	"  -C, --directory=DIR      change to DIR\n",
	"  -k, --kill-mode=MODE     set program termination mode: group, program,",
        "                           or mixed",
        "  -t, --timeout=SECONDS    time to wait for the program to start up or",
        "                           terminate; to set timeouts separately, use",
	"                           start=N or stop=N as argument; the two can be",
        "                           specified together, as in: start=N,stop=N",
	"  -P, --pid-from=SOURCE    where to look for PIDs of the running programs",
	"  -q, --quiet              suppress normal diagnostic messages",
	"  -F, --pidfile=NAME       name of the PID file",
	"                           (same as --pid-from=FILE:NAME)",
	"      --signal-reload=SIG  signal to send on reload (default: SIGHUP)",
	"                           setting to 0 is equivalent to --no-reload",
	"      --no-reload          makes reload equivalent to restart",
	"      --signal-stop=SIG    signal to send in order to terminate the program",
	"                           (default: SIGTERM)",
	"  -v. --verbose            enable verbose diagnostics",
	"",
	"Sentinel mode:",
	"",
	"  -S, --sentinel           PROGRAM runs in foreground; disconnect from the",
	"                           controlling terminal, run it and act as a sentinel",
	"      --create-pidfile=FILE",
	"                           store PID of the PROGRAM in FILE; implies",
	"                           --pid-from=FILE:FILENAME, unless --pid-from is also",
	"                           given",
	"  -s, --shell=SHELL        use SHELL instead of /bin/sh;",
	"                           --shell=none to exec PROGRAM directly",
	"  -e, --exec               same as --shell=none",
	"      --restart-on-exit=[!]CODE[,...]",
	"                           restart the program if it exits with one of the",
	"                           listed status codes",
	"      --restart-on-signal=[!]SIG[,...]",
	"                           restart the program if it terminates on one of the",
	"                           listed signals",
	"      --log-facility=FACILITY",
	"                           log diagnostic messages to this syslog facility",
	"      --log-tag=TAG        use this syslog tag instead of the program name",
	"      --log-file=FILE      send command's stdout and stderr to FILE",
	"      --log-err-file=FILE  send command's stderr to FILE",
	"      --log-out-file=FILE  send command's stdout to FILE",
	"",
	"Informational options:",
	"",
	"  -h, --help               display this help list",
	"      --usage              display short usage information",
	"      --version            display program version and exist",
	"",
	"Influential environment variables and corresponding options:",
	"",
	"   " ENV_GENRC_COMMAND       "=COMMAND   --command=COMMAND",
	"   " ENV_GENRC_GROUP         "=GROUPS      --group=GROUPS",
	"   " ENV_GENRC_KILL_MODE     "=MODE    --kill-mode=MODE",
	"   " ENV_GENRC_LOG_ERR_FILE  "=FILE --log-err-file=FILE",
	"   " ENV_GENRC_LOG_FACILITY  "=F    --log-facility=F",
	"   " ENV_GENRC_LOG_FILE      "=FILE     --log-file=FILE",
	"   " ENV_GENRC_LOG_OUT_FILE  "=FILE --log-out-file=FILE",
	"   " ENV_GENRC_LOG_TAG       "=STR       --log-tag=STR",
	"   " ENV_GENRC_PID_FROM      "=SOURCE   --pid-from=SOURCE",
	"   " ENV_GENRC_PROGRAM       "=NAME      --program=NAME",
	"   " ENV_GENRC_SENTINEL      "=1        --sentinel",
	"   " ENV_GENRC_TIMEOUT       "=SECONDS   --timeout=SECONDS",
	"   " ENV_GENRC_USER          "=NAME         --user=NAME",
	"",
	"PID sources:",
	"",
	"   FILE:<NAME>",
	"      Read PID from the file <NAME>",
	"",
	"   CONFIG:<LANG>:<FILENAME>:<FQRN>",
	"      Name of the PID file is stored in relation <FQRN> of the configuration",
	"      file <FILENAME>, written in language <LANG>",
	"",
	"   GREP:<FILE>:s/<RX>/<REPL>/[<FLAGS>][;...]",
	"      Grep for the first line in <FILE> that matches <RX>. If found,",
	"      replace the matched portion according to <REPL> and <FLAGS>. Use",
	"      the resulting string as PID. More sed expressions can be supplied",
	"      separated with semicolons.",
	"",
	"   PROC[:[<EXE>][:<FLAGS>]]",
	"      Look for matching program in /proc/<PID>/*. If <EXE> is not supplied",
	"      or empty, program name from --program will be used. <FLAGS> are:",
	"        e        exact match",
	"        g        glob pattern match",
	"        x        extended POSIX regexp match (default)",
	"        p        PCRE match",
	"        i        case-insensitive match",
	"",
	"        c        match entire command line",
	"        r        match real executable name (instead of argv0)",
	"        a        signal all matching PIDs",
	"",
	"   PS[:[<EXE>][:<FLAGS>]]",
	"      Look for matching program in the output of 'ps -efw'. <EXE> and <FLAGS>",
	"      as described above",
	"",
	"Default PID source: " DEFAULT_PID_SOURCE,
	"",
	"Report bugs to <" PACKAGE_BUGREPORT ">",
	PACKAGE_NAME " home page: <" PACKAGE_URL ">",
	NULL
};

void
help(void)
{
	int i;

	for (i = 0; help_msg[i]; i++)
		puts(help_msg[i]);
}

char const *usage_msg[] = {
	"genrc",
	"[-hev]",
	"[-C DIR]",
	"[-F PIDFILE]",
	"[-P SOURCE]",
	"[-c COMMAND]",
	"[-g GROUP[,GROUP...]]",
	"[-k MODE]",
	"[-l LIM]",
	"[-p PROGRAM]",
	"[-s SHELL]",
	"[-t SECONDS]",
	"[-u USER]",
	"[--command=COMMAND]",
	"[--directory=DIR]",
	"[--create-pidfile=FILE]",
	"[--exec]",
	"[--group GROUP[,GROUP...]]",
	"[--help]",
	"[--kill-mode=MODE]",
	"[--limit=LIM]",
	"[--log-err-file=FILE]",
	"[--log-facility=FACILITY]",
	"[--log-out-file=FILE]",
	"[--log-tag=TAG]",
	"[--no-reload]",
	"[--pid-from=SOURCE]",
	"[--pidfile=PIDFILE]",
	"[--program=PROGRAM]",
	"[--restart-on-exit=[!]CODE[,...]]",
	"[--restart-on-signal=[!]SIG[,...]]",
	"[--sentinel]",
	"[--shell=SHELL]",
	"[--signal-reload=SIG]",
	"[--signal-stop=SIG]",
	"[--timeout=SECONDS]",
	"[--usage]",
	"[--user=USER]",
	"[--verbose]",
	"[--version]",
	"{",
	"loghup",
	"|",
	"status",
	"|",
	"start",
	"|",
	"stop",
	"|",
	"restart",
	"|",
	"reload",
	"}",
	NULL
};

static int
screen_width(void)
{
	struct winsize ws;
	ws.ws_col = ws.ws_row = 0;
	if ((ioctl(1, TIOCGWINSZ, (char *) &ws) < 0) || ws.ws_col == 0) {
		char *p = getenv("COLUMNS");
		if (p)
			ws.ws_col = atoi(p);
		if (ws.ws_col == 0)
			ws.ws_col = 80;
	}
	return ws.ws_col;
}

enum {
	LIM_PRIO,
	LIM_CORE,
	LIM_DATA,
	LIM_FSIZE,
	LIM_MEMLOCK,
	LIM_RSS,
	LIM_NOFILE,
	LIM_STACK,
	LIM_CPU,
	LIM_NPROC,
	LIM_AS,

	NLIM
};

static struct genrc_resource {
	int letter;
	int resource;
	long mul;
	int isset;
	int force;
	long value;
} resource_tab[] = {
	{ 'p', 0, 0 },
	{ 'c', RLIMIT_CORE, 1024 },
	{ 'd', RLIMIT_DATA, 1024 },
	{ 'f', RLIMIT_FSIZE, 1024  },
	{ 'l', RLIMIT_MEMLOCK, 1024 },
	{ 'm', RLIMIT_RSS, 1024 },
	{ 'n', RLIMIT_NOFILE, 1 },
	{ 's', RLIMIT_STACK, 1024 },
	{ 't', RLIMIT_CPU, 1 },
	{ 'u', RLIMIT_NPROC, 1 },
	{ 'v', RLIMIT_AS, 1024},
};

void
setlimits(void)
{
	int i;

	for (i = 0; i < NLIM; i++) {
		if (resource_tab[i].isset) {
			if (i == LIM_PRIO) {
				if (setpriority(PRIO_PROCESS, 0,
						resource_tab[i].value)) {
					genrc_error("failed to set process priority: %s",
						    strerror(errno));
					if (resource_tab[i].force)
						_exit(126);
				}
			} else {
				struct rlimit rlim;

				rlim.rlim_cur = rlim.rlim_max = resource_tab[i].value;
				if (setrlimit(resource_tab[i].resource, &rlim)) {
					genrc_error("%c: failed to set limit: %s",
						    resource_tab[i].letter,
						    strerror(errno));
					if (resource_tab[i].force)
						_exit(126);
				}
			}
		}
	}
}

static int
find_resource(int letter)
{
	int i;
	letter = tolower(letter);
	for (i = 0; i < sizeof(resource_tab)/sizeof(resource_tab[0]); i++)
		if (letter == resource_tab[i].letter)
			return i;
	return -1;
}

static void
parse_limits(char *arg)
{
	int r;
	char *p;
	long v;

	for (;;) {
		while (*arg == ' ' || *arg == '\t')
			arg++;
		if (*arg == 0)
			break;

		if ((r = find_resource(arg[0])) == -1) {
			usage_error("%s: unrecognized resource code", arg);
		}

		errno = 0;
		v = strtol(arg + 1, &p, 10);
		if (errno) {
			usage_error("%s: resource value out of range", arg);
		}

		if (r == LIM_PRIO) {
			if (v < -20 || v > 19)
				usage_error("%s: resource value out of range", arg);
		} else {
			if (v < 0)
				usage_error("%s: resource value out of range", arg);
			v *= resource_tab[r].mul;
		}
		resource_tab[r].isset = 1;
		resource_tab[r].force = isupper(arg[0]);
		resource_tab[r].value = v;
		arg = p;
	}
}

int
envareq(char const *name, char const *value)
{
	char const *p;
	return (p = getenv(name)) != NULL && strcmp(p, value) == 0;
}

void
usage(void)
{
	int i, j, pos = 0;
	int width = screen_width();
	if (width > 4)
		width -= 4;
	for (i = j = 0; usage_msg[i]; i++) {
		int len = strlen(usage_msg[i]);
		if (j > 0)
			len++;
		if (pos + len > width) {
			putchar('\n');
			pos = strlen(usage_msg[0]) + 1;
			printf("%*s", pos, "");
			j = 0;
		} else if (j > 0)
			putchar(' ');
		j++;
		printf("%s", usage_msg[i]);
		pos += len;
	}
	putchar('\n');
}

static const char gplv3[] =
	"License GPLv3+: GNU GPL version 3 or later "
	"<http://gnu.org/licenses/gpl.html>\n"
	"This is free software: you are free to change and redistribute it.\n"
	"There is NO WARRANTY, to the extent permitted by law.\n\n";

void
version(void)
{
	printf("%s\n", PACKAGE_STRING);
	printf("Copyryght (C) 2018-2024 Sergey Poznyakoff\n");
	printf("%s", gplv3);
}

static struct facility_def {
	char const *name;
	int facility;
} facility_tab[] = {
	{ "auth",    LOG_AUTH },
	{ "authpriv",LOG_AUTHPRIV },
	{ "cron",    LOG_CRON },
	{ "daemon",  LOG_DAEMON },
	{ "ftp",     LOG_FTP },
	{ "lpr",     LOG_LPR },
	{ "mail",    LOG_MAIL },
	{ "user",    LOG_USER },
	{ "local0",  LOG_LOCAL0 },
	{ "local1",  LOG_LOCAL1 },
	{ "local2",  LOG_LOCAL2 },
	{ "local3",  LOG_LOCAL3 },
	{ "local4",  LOG_LOCAL4 },
	{ "local5",  LOG_LOCAL5 },
	{ "local6",  LOG_LOCAL6 },
	{ "local7",  LOG_LOCAL7 },
	{ NULL }
};

static int
str_to_facility(char const *str)
{
	int i;
	for (i = 0; facility_tab[i].name; i++)
		if (strcasecmp(str, facility_tab[i].name) == 0)
			return facility_tab[i].facility;
	return -1;
}

void
genrc_openlog(void)
{
	openlog(genrc_log_tag, LOG_PID, genrc_log_facility);
}

typedef int (*GENRC_COMMAND)(void);

struct comtab {
	char const *com_name;
	GENRC_COMMAND com_fun;
};

static struct comtab comtab[] = {
	{ "start",   com_start },
	{ "stop",    com_stop },
	{ "restart", com_restart },
	{ "reload",  com_reload },
	{ "status",  com_status },
	{ "loghup",  com_loghup },
	{ "wait",    com_wait },
	{ NULL, NULL }
};

GENRC_COMMAND
find_command(char const *s)
{
	struct comtab *c;
	for (c = comtab; c->com_name; c++)
		if (strcmp(c->com_name, s) == 0)
			break;
	return c->com_fun;
}

extern char **environ;

int
main(int argc, char **argv)
{
	int c;
	char *p;
	int no_reload = 0;
	GENRC_COMMAND command;

	setprogname(argv[0]);
	mf_proctitle_init (argc, argv, environ);

	shortopts_setup();
	while ((c = getopt_long(argc, argv, shortopts, longopts, NULL))
	       != EOF) {
		switch (c) {
		case 'h':
			help();
			exit(0);
		case OPT_USAGE:
			usage();
			exit(0);
		case OPT_VERSION:
			version();
			exit(0);
		case 'C':
			if (chdir(optarg)) {
				genrc_error("%s: %s", optarg, strerror(errno));
				return 1;
			}
			break;
		case 'c':
			setenv(ENV_GENRC_COMMAND, optarg, 1);
			break;
		case 'e':
			genrc_shell = NULL;
			break;
		case 'k':
			setenv(ENV_GENRC_KILL_MODE, optarg, 1);
			break;
		case 'l':
			parse_limits(optarg);
			break;
		case 's':
			if (strcmp(optarg, "no") == 0
			    || strcmp(optarg, "none") == 0)
				genrc_shell = NULL;
			else
				genrc_shell = optarg;
			break;
		case 'p':
			setenv(ENV_GENRC_PROGRAM, optarg, 1);
			break;
		case 'P':
			setenv(ENV_GENRC_PID_FROM, optarg, 1);
			break;
		case 'F':
			p = xmalloc(6 + strlen(optarg));
			strcat(strcpy(p, "FILE:"), optarg);
			setenv(ENV_GENRC_PID_FROM, p, 1);
			free(p);
			break;
		case 'g':
			setenv(ENV_GENRC_GROUP, optarg, 1);
			break;
		case 'q':
			genrc_verbose = 0;
			break;
		case OPT_CREATE_PIDFILE:
			setenv(ENV_GENRC_CREATE_PIDFILE, optarg, 1);
			break;
		case OPT_LOG_FACILITY:
			setenv(ENV_GENRC_LOG_FACILITY, optarg, 1);
			break;
		case OPT_LOG_TAG:
			setenv(ENV_GENRC_LOG_TAG, optarg, 1);
			break;
		case OPT_LOG_ERR_FILE:
			setenv(ENV_GENRC_LOG_ERR_FILE, optarg, 1);
			break;
		case OPT_LOG_OUT_FILE:
			setenv(ENV_GENRC_LOG_OUT_FILE, optarg, 1);
			break;
		case OPT_LOG_FILE:
			setenv(ENV_GENRC_LOG_FILE, optarg, 1);
			break;
		case 't':
			setenv(ENV_GENRC_TIMEOUT, optarg, 1);
			break;
		case 'S':
			setenv(ENV_GENRC_SENTINEL, "1", 1);
			break;
		case OPT_RESTART_ON_EXIT:
			add_restart_condition(RESTART_ON_EXIT, optarg);
			break;
		case OPT_RESTART_ON_SIGNAL:
			add_restart_condition(RESTART_ON_SIGNAL, optarg);
			break;
		case OPT_NO_RELOAD:
			no_reload = 1;
			break;
		case OPT_SIGNAL_RELOAD:
			setenv(ENV_GENRC_SIGNAL_RELOAD, optarg, 1);
			break;
		case OPT_SIGNAL_STOP:
			setenv(ENV_GENRC_SIGNAL_STOP, optarg, 1);
			break;
		case 'u':
			setenv(ENV_GENRC_USER, optarg, 1);
			break;
		case 'v':
			genrc_verbose++;
			break;
		default:
			exit(1);
		}
	}

	if ((p = getenv(ENV_GENRC_COMMAND)) != NULL)
		genrc_command = p;
	if ((p = getenv(ENV_GENRC_PROGRAM)) != NULL)
		genrc_program = p;

	if (no_reload)
		genrc_no_reload = 1;
	else if ((p = getenv(ENV_GENRC_SIGNAL_RELOAD)) != NULL) {
		genrc_signal_reload = str_to_sig(p);
		if (genrc_signal_reload == -1)
			usage_error("%s: invalid signal number", p);
		else if (genrc_signal_reload == 0)
			genrc_no_reload = 1;
	}

	if ((p = getenv(ENV_GENRC_SIGNAL_STOP)) != NULL) {
		genrc_signal_stop = str_to_sig(p);
		if (genrc_signal_stop <= 0)
			usage_error("%s: invalid signal number", p);
	}

	if ((p = getenv(ENV_GENRC_KILL_MODE)) != NULL) {
		if (strcmp(p, "group") == 0)
			genrc_kill_mode = genrc_kill_group;
		else if (strcmp(p, "process") == 0)
			genrc_kill_mode = genrc_kill_process;
		else if (strcmp(p, "mixed") == 0)
			genrc_kill_mode = genrc_kill_mixed;
		else
			usage_error("%s: invalid kill mode", p);
	}

	if ((p = getenv(ENV_GENRC_TIMEOUT)) != NULL) {
		char *end;
		unsigned long n;

		if (isdigit(*p)) {
			errno = 0;
			n = strtoul(p, &end, 10);
			if (errno || *end || n > UINT_MAX)
				usage_error("%s: invalid timeout", p);
			if (n == 0)
				genrc_no_reload = 1;
			else
				genrc_start_timeout = genrc_stop_timeout = n;
		} else {
			while (*p) {
				unsigned *t;
				if (strncmp(p, "start=", 6) == 0) {
					t = &genrc_start_timeout;
					p += 6;
				} else if (strncmp(p, "stop=", 5) == 0) {
					t = &genrc_stop_timeout;
					p += 5;
				} else
					usage_error("%s: invalid timeout", p);

				errno = 0;
				n = strtoul(p, &end, 10);
				if (errno || n > UINT_MAX)
					usage_error("%s: invalid timeout", p);
				if (n > 0)
					*t = n;
				p = end;
				if (*p == ',')
					p++;
				else if (*p != 0)
					usage_error("garbage after timeout (near %s)", p);
			}
		}
	}

	if (!genrc_command) {
		if (genrc_program) {
			genrc_command = xstrdup(genrc_program);
		} else {
			usage_error("either --command (GENRC_COMMAND) or --program (GENRC_PROGRAM) must be given");
		}
	} else if (!genrc_program) {
		struct wordsplit ws;
		ws.ws_error = genrc_error;
		if (wordsplit(genrc_command, &ws,
			      WRDSF_NOCMD|WRDSF_NOVAR|
			      WRDSF_ENOMEMABRT|WRDSF_SHOWERR|WRDSF_ERROR))
			exit(1);
		genrc_program = xstrdup(ws.ws_wordv[0]);
		wordsplit_free(&ws);
	}

	genrc_create_pidfile = getenv(ENV_GENRC_CREATE_PIDFILE);

	if ((p = getenv(ENV_GENRC_PID_FROM)) != NULL) {
		genrc_pid_closure = get_pid_closure(p);
	} else if (genrc_create_pidfile) {
		p = xmalloc(6 + strlen(genrc_create_pidfile));
		strcat(strcpy(p, "FILE:"), genrc_create_pidfile);
		genrc_pid_closure = get_pid_closure(p);
		free(p);
	} else {
		genrc_pid_closure = get_pid_closure(DEFAULT_PID_SOURCE);
	}

	if ((p = getenv(ENV_GENRC_LOG_FACILITY)) != NULL) {
		if ((c = str_to_facility(p)) == -1)
			usage_error("%s: unknown facility", p);
		genrc_log_facility = c;
	}
	if ((p = getenv(ENV_GENRC_LOG_TAG)) != NULL)
		genrc_log_tag = p;
	else
		genrc_log_tag = genrc_program;

	if (getenv(ENV_GENRC_LOG_FILE) != NULL) {
		if (getenv(ENV_GENRC_LOG_ERR_FILE) != NULL)
			usage_error("both --log-file and --log-err-file are set");
		if (getenv(ENV_GENRC_LOG_OUT_FILE) != NULL)
			usage_error("both --log-file and --log-out-file are set");
	}

	argc -= optind;
	if (argc == 0)
		usage_error("missing command verb");
	if (argc > 1)
		usage_error("too many arguments");
	argv += optind;

	command = find_command(argv[0]);
	if (!command)
		usage_error("unrecognized command: %s", argv[0]);
	exit(command());
}
