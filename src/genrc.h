/* This file is part of genrc
Copyryght (C) 2018-2024 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <limits.h>
#include <errno.h>
#include <string.h>
#include <regex.h>
#include <ctype.h>
#include <sys/time.h>
#include <sys/select.h>
#include <sys/wait.h>
#include "grecs.h"
#include "wordsplit.h"

void setprogname(char const *s);
void genrc_error(char const *fmt, ...);
void usage_error(char const *fmt, ...);
void system_error(int ec, char const *fmt, ...);

#define xmalloc grecs_malloc
#define xzalloc grecs_zalloc
#define xcalloc grecs_calloc
#define xrealloc grecs_realloc
#define xstrdup grecs_strdup
void *x2nrealloc(void *p, size_t *pn, size_t s);

int envareq(char const *name, char const *value);

#define SHELL "/bin/sh"

pid_t file_read_pid(char const *filename);

typedef struct transform *TRANSFORM;
char *transform_string(TRANSFORM tf, const char *input);
char *transform_string_if_match(TRANSFORM tf, const char *input);
TRANSFORM compile_transform_expr(const char *expr, int cflags);

struct pidlist {
	pid_t *pidv;
	size_t pidc;
	size_t pidn;
};
typedef struct pidlist PIDLIST;

void pidlist_init(PIDLIST *);
void pidlist_free(PIDLIST *);
void pidlist_clear(PIDLIST *plist);
void pidlist_add(PIDLIST *, pid_t);
ssize_t pidlist_index(PIDLIST *plist, pid_t p);
int pidlist_member(PIDLIST *plist, pid_t p);
int pidlist_remove(PIDLIST *plist, size_t i);
void pidlist_kill(PIDLIST *plist, int sig);

pid_t strtopid(char const *str);

int pid_is_running(pid_t pid);

void setlimits(void);

void runas(void);
int str_to_sig(char const *);
int str_to_int(char const *);

enum {
	MATCH_REGEX,  /* extended POSIX regexp match (default) */
	MATCH_PCRE,   /* PCRE match (not implemented) */
	MATCH_GLOB,   /* glob pattern match */
	MATCH_EXACT,  /* exact match */
};
#define MATCH_DEFAULT MATCH_REGEX

enum {
	PROCF_ICASE   = 0x01, /* ignore case */
	PROCF_ALL     = 0x02, /* use all pids */
	PROCF_CMDLINE = 0x04, /* match against entire command line */
	PROCF_EXE     = 0x08  /* match against executable name */
};
#define PROCF_DEFAULT 0

struct procscanbuf {
	int match;   /* match type */
	int flags;   /* match flags */
	void *pattern;
};

typedef struct procscanbuf *PROCSCANBUF;

PROCSCANBUF procscan_init(char const *pattern, char const *flagstr);
void procscan_free(PROCSCANBUF buf);
int procscan_match(PROCSCANBUF buf, char const *arg);

void match_exact_init(PROCSCANBUF buf, char const *pattern);
void match_exact_free(PROCSCANBUF buf);
int match_exact(PROCSCANBUF buf, char const *arg);

void match_glob_init(PROCSCANBUF buf, char const *pattern);
void match_glob_free(PROCSCANBUF buf);
int match_glob(PROCSCANBUF buf, char const *arg);

void match_regex_init(PROCSCANBUF buf, char const *pattern);
void match_regex_free(PROCSCANBUF buf);
int match_regex(PROCSCANBUF buf, char const *arg);

void match_pcre_init(PROCSCANBUF buf, char const *pattern);
void match_pcre_free(PROCSCANBUF buf);
int match_pcre(PROCSCANBUF buf, char const *arg);


enum {
	RESTART_ON_EXIT,
	RESTART_ON_SIGNAL
};

void add_restart_condition(int type, char const *arg);


struct genrc_pid_closure {
	char const *name;
	int (*pid)(struct genrc_pid_closure *, PIDLIST *);
};

typedef struct genrc_pid_closure GENRC_PID_CLOSURE;
GENRC_PID_CLOSURE *get_pid_closure(char const *str);
int get_pid_list(GENRC_PID_CLOSURE *, PIDLIST *);
pid_t get_genrc_pid(pid_t pid);

GENRC_PID_CLOSURE *genrc_pid_file_init(int argc, char **argv);
GENRC_PID_CLOSURE *genrc_pid_config_init(int argc, char **argv);
GENRC_PID_CLOSURE *genrc_pid_grep_init(int argc, char **argv);
GENRC_PID_CLOSURE *genrc_pid_proc_init(int argc, char **argv);
GENRC_PID_CLOSURE *genrc_pid_ps_init(int argc, char **argv);

extern char *genrc_command;
extern char *genrc_program;
extern char *genrc_pid_from;
extern unsigned genrc_start_timeout, genrc_stop_timeout;
extern int genrc_no_reload;
extern int genrc_signal_reload;
extern int genrc_signal_stop;
extern GENRC_PID_CLOSURE *genrc_pid_closure;
extern char *genrc_create_pidfile;
extern int genrc_verbose;
extern char *genrc_shell;

void genrc_openlog(void);

void spawn(int *p);

int sentinel(void);

int com_start(void);
int com_status(void);
int com_stop(void);
int com_restart(void);
int com_reload(void);
int com_loghup(void);
int com_wait(void);

enum genrc_kill_mode {
	genrc_kill_group,
	genrc_kill_process,
	genrc_kill_mixed
};

extern enum genrc_kill_mode genrc_kill_mode;

void mf_proctitle_init (int argc, char *argv[], char *env[]);
void mf_proctitle_format (const char *fmt, ...);
void close_fds_from(int minfd);

#define ENV_GENRC_COMMAND "GENRC_COMMAND"
#define ENV_GENRC_CREATE_PIDFILE "GENRC_CREATE_PIDFILE"
#define ENV_GENRC_LOG_FILE "GENRC_LOG_FILE"
#define ENV_GENRC_GROUP "GENRC_GROUP"
#define ENV_GENRC_KILL_MODE "GENRC_KILL_MODE"
#define ENV_GENRC_LOG_ERR_FILE "GENRC_LOG_ERR_FILE"
#define ENV_GENRC_LOG_FACILITY "GENRC_LOG_FACILITY"
#define ENV_GENRC_LOG_OUT_FILE "GENRC_LOG_OUT_FILE"
#define ENV_GENRC_LOG_TAG "GENRC_LOG_TAG"
#define ENV_GENRC_PID_FROM "GENRC_PID_FROM"
#define ENV_GENRC_PROGRAM "GENRC_PROGRAM"
#define ENV_GENRC_SENTINEL "GENRC_SENTINEL"
#define ENV_GENRC_SIGNAL_RELOAD "GENRC_SIGNAL_RELOAD"
#define ENV_GENRC_SIGNAL_STOP "GENRC_SIGNAL_STOP"
#define ENV_GENRC_TIMEOUT "GENRC_TIMEOUT"
#define ENV_GENRC_USER "GENRC_USER"
