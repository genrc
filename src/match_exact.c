/* This file is part of genrc
Copyryght (C) 2018-2024 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include "genrc.h"

void
match_exact_init(PROCSCANBUF buf, char const *pattern)
{
	buf->pattern = xstrdup(pattern ? pattern : genrc_program);
}

void
match_exact_free(PROCSCANBUF buf)
{
	free(buf->pattern);
}

int
match_exact(PROCSCANBUF buf, char const *arg)
{
	return ((buf->flags & PROCF_ICASE) ? strcasecmp : strcmp)
		((char*)buf->pattern, arg);
}
