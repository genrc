/* This file is part of genrc
Copyryght (C) 2018-2024 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include "genrc.h"

int
com_reload(void)
{
	PIDLIST pids;

	if (genrc_no_reload)
		return com_restart();
	pidlist_init(&pids);
	if (get_pid_list(genrc_pid_closure, &pids))
		return 1;
	if (genrc_verbose > 1) {
		printf("Reloading %s: sending %s to %s ",
		       genrc_program, strsignal(genrc_signal_reload),
		       pids.pidc > 1 ? "PIDs" : "PID");
		printf("%lu", (unsigned long)pids.pidv[0]);
		if (pids.pidc > 1) {
			int i;
			for (i = 0; i < pids.pidc; i++ )
				printf(",%lu", (unsigned long)pids.pidv[i]);
		}
		putchar('\n');
	}
	pidlist_kill(&pids, genrc_signal_reload);
	return 0;
}
