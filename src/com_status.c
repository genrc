/* This file is part of genrc
Copyryght (C) 2018-2024 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include "genrc.h"

int
com_status(void)
{
	PIDLIST pids;
	int rc;

	pidlist_init(&pids);
	rc = get_pid_list(genrc_pid_closure, &pids);
	if (rc == 0) {
		if (genrc_verbose == 0)
			return pids.pidc == 0;
		if (pids.pidc == 0) {
			printf("%s is not running\n", genrc_program);
			return 1;
		} else {
			printf("%s is running ", genrc_program);
			if (pids.pidc == 1) {
				printf("(pid %lu)",
				       (unsigned long) pids.pidv[0]);
			} else {
				int i;
				int delim = ' ';
				printf("(pids");
				for (i = 0; i < pids.pidc; i++) {
					printf("%c%lu", delim,
					       (unsigned long) pids.pidv[i]);
					delim = ',';
				}
				putchar(')');
			}
			putchar('\n');
		}
	} else {
		if (genrc_verbose)
			printf("status unknown\n");
		return 2;
	}
	return 0;
}
