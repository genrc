/* This file is part of genrc
Copyryght (C) 2024 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include "genrc.h"

int
com_wait(void)
{
	PIDLIST pids;
	struct timeval start, stop;
	
	gettimeofday(&start, NULL);
	pidlist_init(&pids);
	gettimeofday(&stop, NULL);
	stop.tv_sec += genrc_stop_timeout;
	for (;;) {
		if (get_pid_list(genrc_pid_closure, &pids))
			return 1;
		if (pids.pidc == 0)
			break;
		gettimeofday(&start, NULL);
		if (timercmp(&start, &stop, >=)) {
			pidlist_free(&pids);
			if (genrc_verbose) {
				printf("timed out waiting for %s to terminate\n",
				       genrc_program);
				if (pids.pidc > 1)
					printf("%zu processes still running\n",
					       pids.pidc);
			}
			return 1;
		}
		sleep(1);
	}

	pidlist_free(&pids);

	if (genrc_verbose)
		printf("%s terminated\n", genrc_program);

	return 0;
}

		
