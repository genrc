/* This file is part of genrc
Copyryght (C) 2018-2024 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include "genrc.h"

struct genrc_pid_source {
	char *name;
	size_t maxwords;
	GENRC_PID_CLOSURE *(*init)(int argc, char **argv);
};

struct genrc_pid_source sourcetab[] = {
	{ "FILE",   2, genrc_pid_file_init },
	{ "CONFIG", 4, genrc_pid_config_init },
	{ "GREP",   3, genrc_pid_grep_init },
	{ "PROC",   3, genrc_pid_proc_init },
	{ "PS",     3, genrc_pid_ps_init },
	{ NULL }
};

GENRC_PID_CLOSURE *
get_pid_closure(char const *str)
{
	struct genrc_pid_source *s;
	struct wordsplit ws;
	GENRC_PID_CLOSURE *pc;
	size_t len = strcspn(str, ":");
	int wsflags;

	if (len == 0)
		usage_error(ENV_GENRC_PID_FROM " argument is empty");
	for (s = sourcetab; ; s++) {
		if (!s->name)
			usage_error("%s: unsupported PID source", str);
		if (strlen(s->name) == len && memcmp(s->name, str, len) == 0)
			break;
	}

	ws.ws_error = genrc_error;
	ws.ws_delim = ":";
	ws.ws_escape[WRDSX_WORD] = ws.ws_escape[WRDSX_QUOTE] = "\\\\::";
	wsflags =  WRDSF_NOCMD
		   | WRDSF_NOVAR
		   | WRDSF_QUOTE
		   | WRDSF_ESCAPE
		   | WRDSF_DELIM
		   | WRDSF_ENOMEMABRT
		   | WRDSF_SHOWERR
		   | WRDSF_ERROR;
	if (s->maxwords) {
		ws.ws_maxwords = s->maxwords;
		ws.ws_options = WRDSO_MAXWORDS;
		wsflags |= WRDSF_OPTIONS;
	}
	if (wordsplit(str, &ws, wsflags))
		exit(1);
	pc = s->init(ws.ws_wordc, ws.ws_wordv);
	if (!pc)
		exit(1);
	pc->name = xstrdup(str);
	return pc;
}

int
get_pid_list(GENRC_PID_CLOSURE *clos, PIDLIST *plist)
{
	pidlist_clear(plist);
	return clos->pid(clos, plist);
}
