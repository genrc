/* This file is part of genrc
Copyryght (C) 2018-2024 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include "genrc.h"

struct flagdfn {
	int letter;   /* flag letter */
	int is_match; /* is this flag a match mode */
	int value;    /* flag value */
};

static struct flagdfn flagtab[] = {
	{ 'e',   1, MATCH_EXACT },
	{ 'g',   1, MATCH_GLOB  },
	{ 'x',   1, MATCH_REGEX },
	{ 'p',   1, MATCH_PCRE  },
	{ 'i',   0, PROCF_ICASE },
	{ 'a',   0, PROCF_ALL },
	{ 'c',   0, PROCF_CMDLINE },
	{ 'r',   0, PROCF_EXE },
	{ 0 }
};

static struct flagdfn *
find_flag(int let)
{
	struct flagdfn *p;

	let = tolower(let);
	for (p = flagtab; p->letter; p++)
		if (p->letter == let)
			return p;
	return NULL;
}

struct match_type {
	void (*match_init)(PROCSCANBUF, char const *);
	void (*match_free)(PROCSCANBUF);
	int (*match_func)(PROCSCANBUF, char const *);
};

struct match_type match_types[] = {
	[MATCH_REGEX] = { match_regex_init, match_regex_free, match_regex },
#ifdef HAVE_PCRE
	[MATCH_PCRE]  = { match_pcre_init, match_pcre_free, match_pcre },
#endif
	[MATCH_GLOB]  = { match_glob_init, match_glob_free, match_glob },
	[MATCH_EXACT] = { match_exact_init, match_exact_free, match_exact }
};

PROCSCANBUF
procscan_init(char const *pattern, char const *flagstr)
{
	PROCSCANBUF buf;

	buf = xmalloc(sizeof(buf));
	buf->match = MATCH_DEFAULT;
	buf->flags = PROCF_DEFAULT;
	if (flagstr) {
		char const *p;
		for (p = flagstr; *p; p++) {
			struct flagdfn *flag = find_flag(*p);
			if (!flag)
				usage_error("%s: unknown flag %c",
					    flagstr, *p);
			if (flag->is_match) {
				if (match_types[flag->value].match_init)
					buf->match = flag->value;
				else
					usage_error("match type '%c' is not available", *p);
			}
			else if (isupper(*p))
				buf->flags &= ~flag->value;
			else
				buf->flags |= flag->value;
		}
	}
	match_types[buf->match].match_init(buf, pattern);
	return buf;
}

void
procscan_free(PROCSCANBUF buf)
{
	match_types[buf->match].match_free(buf);
	free(buf);
}

int
procscan_match(PROCSCANBUF buf, char const *arg)
{
	if (!arg)
		return 1;
	return match_types[buf->match].match_func(buf, arg);
}
