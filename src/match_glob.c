/* This file is part of genrc
Copyryght (C) 2018-2024 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include "genrc.h"
#define _GNU_SOURCE 1
#include <fnmatch.h>

void
match_glob_init(PROCSCANBUF buf, char const *pattern)
{
	buf->pattern = xstrdup(pattern ? pattern : genrc_program);
}

void
match_glob_free(PROCSCANBUF buf)
{
	free(buf->pattern);
}

int
match_glob(PROCSCANBUF buf, char const *arg)
{
	return fnmatch((char*)buf->pattern, arg,
		       (buf->flags & PROCF_ICASE) ? FNM_CASEFOLD : 0);
}
